import { Request, Response } from "express";
import {getAllRewards} from "../service/rewards.service";

export async function getAllRewardsHandler(req: Request, res: Response) {
  const post = await getAllRewards();

  if (!post) {
    return res.sendStatus(404);
  }

  return res.send(post);
}
