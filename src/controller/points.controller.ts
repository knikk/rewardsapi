import { Request, Response } from "express";
import {createPoints, getAllPoints} from "../service/point.service";
import {get} from "lodash";

export async function createPointsHandler(req: Request, res: Response) {
  const body = req.body;

  const points = await createPoints({ ...body });

  return res.send(points);
}

export async function getAllPointsHandler(req: Request, res: Response) {
  const userId = get(req, "params.userId");
  const points = await getAllPoints(userId);
  if (!points) {
    return res.sendStatus(404);
  }

  return res.send(points);
}

