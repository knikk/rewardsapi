import mongoose from "mongoose";
import { nanoid } from "nanoid";

export interface RewardsDocument extends mongoose.Document {
  provider: string;
  location:string;
  rating:number;
  price:number;
  image:string;
}

const RewardsSchema = new mongoose.Schema(
  {
      provider: { type: String },
      location: { type: String},
      rating: { type: String },
      price: { type: String },
      image: { type: String },
  },
  { timestamps: true }
);

const Rewards = mongoose.model<RewardsDocument>("Rewards", RewardsSchema);

export default Rewards;
