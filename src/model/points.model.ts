import mongoose from "mongoose";

export interface PointsDocument extends mongoose.Document {
  userId:string,
  provider: string;
  location:string;
  price:number;
  image:string;
}

const PointsSchema = new mongoose.Schema(
  {
    userId: {
      type: String,
    },
      provider: { type: String },
      location: { type: String},
      price: { type: Number },
      image: { type: String },
  },
  { timestamps: true }
);

const Points = mongoose.model<PointsDocument>("Points", PointsSchema);

export default Points;
