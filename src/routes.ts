import { Express, Request, Response } from "express";
import { validateRequest } from "./middleware";
import {createPointSchema} from "./schema/points.schema";
import {createPointsHandler, getAllPointsHandler} from "./controller/points.controller";
import {getAllRewardsHandler} from "./controller/rewards.controller";

export default function (app: Express) {
  app.get("/healthcheck", (req: Request, res: Response) => res.sendStatus(200));

  // Create a points
  app.post(
      "/api/points",
      [validateRequest(createPointSchema)],
      createPointsHandler
  );

  // Get all points
  app.get(
      "/api/points/:userId",
      getAllPointsHandler
  );
  // Get all rewards
  app.get("/api/rewards", getAllRewardsHandler);
}
