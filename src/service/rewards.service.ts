import {
  FilterQuery,
  UpdateQuery,
  QueryOptions,
} from "mongoose";
import Rewards , { RewardsDocument } from "../model/rewards.model";

export function findReward(
  query: FilterQuery<RewardsDocument>,
  options: QueryOptions = { lean: true }
) {
  return Rewards.findOne(query, {}, options);
}
export function getAllRewards(
) {
  return Rewards.find();
}

export function findAndUpdate(
  query: FilterQuery<RewardsDocument>,
  update: UpdateQuery<RewardsDocument>,
  options: QueryOptions
) {
  return Rewards.findOneAndUpdate(query, update, options);
}

export function deleteReward(query: FilterQuery<RewardsDocument>) {
  return Rewards.deleteOne(query);
}
