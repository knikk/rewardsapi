import {
  DocumentDefinition,
  FilterQuery,
  UpdateQuery,
  QueryOptions,
} from "mongoose";
import Points , { PointsDocument } from "../model/points.model";

export function createPoints(input: DocumentDefinition<PointsDocument>) {
  return Points.create(input);
}

export function findPoints(
  query: FilterQuery<PointsDocument>,
  options: QueryOptions = { lean: true }
) {
  return Points.findOne(query, {}, options);
}
export function getAllPoints(userId:string) {
  return Points.find({userId});
}

export function findAndUpdate(
  query: FilterQuery<PointsDocument>,
  update: UpdateQuery<PointsDocument>,
  options: QueryOptions
) {
  return Points.findOneAndUpdate(query, update, options);
}

export function deletePoints(query: FilterQuery<PointsDocument>) {
  return Points.deleteOne(query);
}
