import {number, object, string} from "yup";

const payload = {
  body: object({
    userId:string().required("user id is required"),
    provider: string().required("Provider is required"),
    location: string()
      .required("location is required"),
    price:number().required("price is required"),
    image:string().required("image is required"),
  }),
};

const params = {
  params: object({
    id: string().required("id is required"),
  }),
};

export const createPointSchema = object({
  ...payload,
});
