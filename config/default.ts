export default {
  port: 1337,
  host: "localhost",
  dbUri: "mongodb+srv://admin:admin1234@cluster0.hgna3.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
  saltWorkFactor: 10,
  accessTokenTtl: "15m",
  refreshTokenTtl: "1y",
};
